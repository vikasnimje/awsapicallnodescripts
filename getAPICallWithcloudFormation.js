require('dotenv').config();
const axios = require('axios');
var CryptoJS = require("crypto-js");
var dt = require('./datefunctions');
var encodedSearch = require('./CanonicalQueryGenerator');
const method = process.env.METHOD_POST;
const host = process.env.HOST_POST;
const endpoint = process.env.ENDPOINT_POST;
const accessKeyId = process.env.ACCESS_KEY;
const secretAccessKey = process.env.SECRET_KEY;
const sessionToken = process.env.SESSION_TOKEN;
const regionName = process.env.REGION_NAME;
const serviceName = process.env.SERVICE_NAME_CLOUDFORMATION;
var payload_hash = ""; // 'payload';
var algorithm = 'AWS4-HMAC-SHA256';
function getSignatureKey(key, datestamp, regionName, serviceName) {
    var kDate = CryptoJS.HmacSHA256(datestamp, "AWS4" + key);
    //console.log('kdate:', kDate);
    var kRegion = CryptoJS.HmacSHA256(regionName, kDate);
    var kService = CryptoJS.HmacSHA256(serviceName, kRegion);
    var kSigning = CryptoJS.HmacSHA256("aws4_request", kService);

    return kSigning;
}
var amzdate = dt.getDateYYYYMMDDHHMMSS();
var datestamp = dt.getDateYYYYMMDD();
const XAmzContentSha256 = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855';
//************* TASK 1: CREATE A CANONICAL REQUEST *************
function getSignedHeader()
{
    return 'host;x-amz-date;x-amz-security-token';
}
function generateCanonicalRequest() {
    const canonical_uri = '/';
    var canonical_querystring = encodedSearch;
    //console.log("canonical_querystring:",  canonical_querystring);
    var canonical_headers = () => {
        return [
            'host:' + host.trim() + '\n',
            'x-amz-date:' + amzdate.trim() + '\n',
            'X-Amz-Security-Token:'.toLowerCase() + sessionToken.trim() + '\n'
        ].join('')
    }
    console.log("canonical_headers:", canonical_headers());
    payload_hash = CryptoJS.SHA256('');
    payload_hash = payload_hash.toString(CryptoJS.enc.Hex);
    //console.log("payload_hash:", payload_hash);
    var canonical_request = () => {
        return [
            method + '\n',
            canonical_uri + '\n',
            canonical_querystring + '\n',
            canonical_headers() + '\n',
            getSignedHeader() + '\n',
            payload_hash

        ].join('')
    }
    //method + '\n' + canonical_uri + '\n' + canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash;
    console.log("canonical_request:\n", canonical_request());
    var x = CryptoJS.SHA256(canonical_request());
    x = x.toString(CryptoJS.enc.Hex);
    console.log("hash value of canonical request:", x);
    return x;
}
// # ************* TASK 2: CREATE THE STRING TO SIGN**************************
function getCredentialScope()
{
    return [
        datestamp + '/',
        regionName + '/',
        serviceName + '/',
        'aws4_request'
    ].join('')
}
function createStringToSign()
{
    
    var stringToSign = algorithm + '\n' + amzdate + '\n' + getCredentialScope() + '\n' + generateCanonicalRequest();
    //var stringToSign = "AWS4-HMAC-SHA256" + '\n' +"20150830T123600Z" +"\n" + "20150830/us-east-1/iam/aws4_request" + "\n" + "f536975d06c0309214f805bb90ccff089219ecd68b2577efef23edd43b7e1a59";
    console.log("String to sign: \n", stringToSign );
    return stringToSign;
}
 // ************* TASK 3: CALCULATE THE SIGNATURE *************
function calculateSignature()
{
    var signingKey = getSignatureKey(secretAccessKey, datestamp, regionName, serviceName);
    var signature = CryptoJS.HmacSHA256(createStringToSign(),signingKey);
    console.log('signature:', signature);
    return signature;
}
//calculateSignature();
// ************* TASK 4: ADD SIGNING INFORMATION TO THE REQUEST *************
function getAuthorizationHeader()
{
    return  [
            algorithm,
            ' ',
            'Credential=',
            accessKeyId,
            '/',
            getCredentialScope(),
            ', ',
            'SignedHeaders=',
            getSignedHeader(),
            ', ',
            'Signature=',
            calculateSignature()

        ].join('')
}
//var authorization =  getAuthorizationHeader();
console.log("authorizationHeader: ", getAuthorizationHeader());
const config = {
    method: method,
    url: endpoint,
    //url: 'http://webcode.me',
    headers: { 
          "Content-Type": "application/json",
          "X-Amz-Security-Token": sessionToken,
           //"X-Amz-Content-Sha256":payload_hash,
            "X-Amz-Date": amzdate,
             "Authorization": getAuthorizationHeader()
    }
     //headers: { 'User-Agent': 'Axios - console app' }
}
axios(config)
.then(function (response) {
console.log(JSON.stringify(response.status));
console.log(response.data);
})
.catch(function (error) {
console.log("Error:", error.response.data);
});