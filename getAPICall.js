require('dotenv').config();
const axios  = require('axios');
var CryptoJS = require("crypto-js");
const crypto = require('crypto');
const AwsSigV4 = require('js-aws-sigv4'); 
var dt = require('./datefunctions');
const regionName = process.env.REGION_NAME;
const serviceName = process.env.SERVICE_NAME_CLOUDFORMATION;

const method = process.env.METHOD_POST;
const host = process.env.HOST_POST;
const endpoint = process.env.ENDPOINT_POST;
const accessKeyId = process.env.ACCESS_KEY;
const secretAccessKey = process.env.SECRET_KEY;
const sessionToken = process.env.SESSION_TOKEN;
var payload_hash = ""; // 'payload';
function getSignatureKey(key, datestamp, regionName, serviceName) {
    var kDate = CryptoJS.HmacSHA256(datestamp, "AWS4" + key);
    //console.log('kdate:', kDate);
    var kRegion = CryptoJS.HmacSHA256(regionName, kDate);
    var kService = CryptoJS.HmacSHA256(serviceName, kRegion);
    var kSigning = CryptoJS.HmacSHA256("aws4_request", kService);
    
    return kSigning;
}

console.log("access_key: ", accessKeyId);
console.log("secret_key:", secretAccessKey);
var amzdate = dt.getDateYYYYMMDDHHMMSS();
console.log("amzdate:", amzdate);
var datestamp = dt.getDateYYYYMMDD();
console.log("datestamp:", datestamp);    

const XAmzContentSha256 = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855';


//************* TASK 1: CREATE A CANONICAL REQUEST *************
// Step 1 is to define the verb (GET, POST, etc.)--already done.
    // Step 2: Create canonical URI--the part of the URI from domain to query
    // string (use '/' if no path)
const canonical_uri = '/';
var canonical_querystring = 'Action=CreateStack&Parameters.member.1.ParameterKey=BucketName&Parameters.member.1.ParameterValue=nilmani-yash-demo9&StackName=nilmanisinghs9&TemplateURL=https%3A%2F%2Fiac-sme-test.s3.amazonaws.com%2Fawsa.yml&Version=2010-05-15';
// Step 4: Create the canonical headers and signed headers. Header names
// must be trimmed and lowercase, and sorted in code point order from
// low to high. Note that there is a trailing \n.
var canonical_headers = 'host:' + host.trim() + '\n'  + 'x-amz-date:' + amzdate.trim() +  '\n' + 'X-Amz-Security-Token:'.toLowerCase() + sessionToken.trim() + '\n'; 
console.log("canonical_headers:", canonical_headers);
//Step 5: Create the list of signed headers. This lists the headers
// in the canonical_headers list, delimited with ";" and in alpha order.
// Note: The request can include any headers; canonical_headers and
// signed_headers lists those that you want to be included in the
// hash of the request. "Host" and "x-amz-date" are always required.
var signed_headers = 'host;x-amz-date;x-amz-security-token';
console.log("signed_headers:", signed_headers);
// Step 6: Create payload hash (hash of the request body content). For GET
// requests, the payload is an empty string ("").
payload_hash = CryptoJS.SHA256('');

payload_hash = payload_hash.toString(CryptoJS.enc.Hex);
console.log("payload_hash:", payload_hash);
// Step 7: Combine elements to create canonical request
var canonical_request = method + '\n' + canonical_uri + '\n' + canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash;
console.log("canonical_request:\n", canonical_request);
var x = CryptoJS.SHA256(canonical_request);
x = x.toString(CryptoJS.enc.Hex);
console.log("hash value of canonical request:", x);
// # ************* TASK 2: CREATE THE STRING TO SIGN**************************
var algorithm = 'AWS4-HMAC-SHA256';
var credential_scope = datestamp + '/' + regionName + '/' + serviceName + '/' + 'aws4_request';
var stringToSign = algorithm + '\n' + amzdate + '\n' + credential_scope + '\n' + x;

//var stringToSign = "AWS4-HMAC-SHA256" + '\n' +"20150830T123600Z" +"\n" + "20150830/us-east-1/iam/aws4_request" + "\n" + "f536975d06c0309214f805bb90ccff089219ecd68b2577efef23edd43b7e1a59";
console.log("String to sign: \n", stringToSign );


// ************* TASK 3: CALCULATE THE SIGNATURE *************
// Create the signing key using the function defined above.
var signingKey = getSignatureKey(secretAccessKey, datestamp, regionName, serviceName);
//var signingKey = getSignatureKey('wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY', '20150830', 'us-east-1', 'iam');
//signingKey = signingKey.toString(CryptoJS.enc.Hex);

//console.log("signingKey:", signingKey);
//var signature = CryptoJS.HmacSHA256(stringToSign.toString(CryptoJS.enc.Utf8), signingKey);
//var signature = CryptoJS.HmacSHA256(stringToSign, signingKey)

//signature = CryptoJS.SHA256(signature);

/*var signature = crypto.createHmac('sha256', signingKey)
                                .update(stringToSign.toString(CryptoJS.enc.Utf8))
                                .digest('Hex');*/
//signature = signature.toString(CryptoJS.enc.Hex);
//var hmac = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA256, signingKey );
//hmac.update(stringToSign);
//var signature = hmac.finalize();
var signature = CryptoJS.HmacSHA256(stringToSign,signingKey)//.toString();
//var signature ="";
//signature = signature.toString(CryptoJS.enc.Hex);
console.log('signature :', signature);
//var signature = '104afbb5989541d199d005b89e8bfd29e55f6b30f07d6a2f1366b6ecbea7891a';
/*async function sig()
{
    const signature = await AwsSigV4.getSignature(
        signingKey,
        stringToSign
      )
    console.log("signature :", signature.toString(CryptoJS.enc.Hex));
}


// ************* TASK 4: ADD SIGNING INFORMATION TO THE REQUEST *************
// The signing information can be either in a query string value or in
// a header named Authorization. This code shows how to use a header.
// Create authorization header and add to request headers
var authorization_header = algorithm + ' ' + 'Credential=' + accessKeyId + '/' + credential_scope + ', ' + 'SignedHeaders=' + signed_headers + ', ' + 'Signature=' + signature; 
console.log("authorizationHeader: ", authorization_header);
/*var headers = {
    'X-Amz-Security-Token': sessionToken,
           'X-Amz-Content-Sha256':payload_hash,
            'X-Amz-Date': amzdate, 'Authorization': authorization_header}
};*/
var authorization_header = algorithm + ' ' + 'Credential=' + accessKeyId + '/' + credential_scope + ', ' + 'SignedHeaders=' + signed_headers + ', ' + 'Signature=' + signature; 
console.log("authorizationHeader: ", authorization_header);
const config = {
    method: method,
    url: endpoint,
    //url: 'http://webcode.me',
    headers: { 
          "Content-Type": "application/json",
          "X-Amz-Security-Token": sessionToken,
           //"X-Amz-Content-Sha256":payload_hash,
            "X-Amz-Date": amzdate,
             "Authorization": authorization_header
    }
     //headers: { 'User-Agent': 'Axios - console app' }
}
axios(config)
.then(function (response) {
console.log(JSON.stringify(response.status));
console.log(response.data);
})
.catch(function (error) {
console.log("Error:", error.response.data);
});
//let res = await axios(config)

//console.log(res.request._header);
//console.log(res.status);
//console.log(res.statusText );
//console.log(res.data);

