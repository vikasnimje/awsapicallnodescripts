require('dotenv').config();
const axios  = require('axios');
var CryptoJS = require("crypto-js");
const regionName = process.env.REGION_NAME;
const serviceName = process.env.SERVICE_NAME;

const method = process.env.METHOD;
const host = process.env.HOST;
const endpoint = process.env.ENDPOINT;
const accessKeyId = process.env.ACCESS_KEY;
const secretAccessKey = process.env.SECRET_KEY;
const sessionToken = process.env.SESSION_TOKEN;
var payload_hash = ""; // 'payload';

console.log('payload_hash:', payload_hash);
/*var headers = { 'X-Amz-Security-Token': "",
'X-Amz-Content-Sha256':"",
 'X-Amz-Date': "",
 'Authorization': ""};*/

function getSignatureKey(key, datestamp, regionName, serviceName) {
    var kDate = CryptoJS.HmacSHA256(datestamp, "AWS4" + key);
    var kRegion = CryptoJS.HmacSHA256(regionName, kDate);
    var kService = CryptoJS.HmacSHA256(serviceName, kRegion);
    var kSigning = CryptoJS.HmacSHA256("aws4_request", kService);
    return kSigning;
}
Date.prototype.yyyymmdd = function() {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();
  
    return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
           ].join('');
  };
Date.prototype.yyyymmddThhmmssZ = function()
{
  var mm = this.getUTCMonth() + 1; // getMonth() is zero-based
  var dd = this.getUTCDate();
  var hh = this.getUTCHours();
  var min = this.getUTCMinutes();
  var ss = this.getUTCSeconds();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd, 
          'T',
          (hh>9 ? '': '0') + hh,
          (min>9 ? '': '0') + min,
          (ss>9 ? '': '0') + ss,
          'Z'
         ].join(''); 
}  
 function getDateYYYYMMDD()
  {
    var date = new Date();
    return date.yyyymmdd();
  }
function getDateYYYYMMDDHHMMSS()
{
    var date = new Date();
    return date.yyyymmddThhmmssZ();
}  
var amzdate = getDateYYYYMMDDHHMMSS();
console.log("amzdate:", amzdate);
var datestamp = getDateYYYYMMDD();
console.log("datestamp:", datestamp);    
const XAmzContentSha256 = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855';





function getCanonicalRequest(XAmzSecurityToken)
{
    // Step 1 is to define the verb (GET, POST, etc.)--already done.
    // Step 2: Create canonical URI--the part of the URI from domain to query
    // string (use '/' if no path)
    const canonical_uri = '/';
    // Step 3: Create the canonical query string. In this example (a GET request),
    canonical_querystring = ''
    // Step 4: Create the canonical headers and signed headers. Header names
    // must be trimmed and lowercase, and sorted in code point order from
    // low to high. Note that there is a trailing \n.
    var canonical_headers = 'host:' + host.trim() + '\n'  + 'X-Amz-Content-Sha256:'.toLowerCase() + XAmzContentSha256.trim() + '\n' + 'x-amz-date:' + amzdate.trim() +  '\n' + 'X-Amz-Security-Token:'.toLowerCase() + XAmzSecurityToken.trim() + '\n'; 
    console.log("canonical_headers:", canonical_headers);
    var signed_headers = 'host;x-amz-content-sha256;x-amz-date;x-amz-security-token';
    payload_hash = CryptoJS.SHA256('');
    
    payload_hash = payload_hash.toString(CryptoJS.enc.Hex);
    console.log("payload_hash:", payload_hash);
    // Step 7: Combine elements to create canonical request
    var canonical_request = method + '\n' + canonical_uri + '\n' + canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash;
    return canonical_request
}
function createStringToSign(x)
{
  var algorithm = 'AWS4-HMAC-SHA256';
  var credential_scope = datestamp + '/' + regionName + '/' + serviceName + '/' + 'aws4_request';
  var stringToSign = algorithm + '\n' + amzdate + '\n' + credential_scope + '\n' + x
  return stringToSign;
}
/*function createSignature(secretKey)
{
  var 
}*/
function getHeaders(accessKey, secretKey, sessionToken)
{
    var signed_headers = 'host;x-amz-content-sha256;x-amz-date;x-amz-security-token';
    var algorithm = 'AWS4-HMAC-SHA256';
    var credentialScope = datestamp + '/' + regionName + '/' + serviceName + '/' + 'aws4_request';
    //var payload_hash = CryptoJS.SHA256('');
    //console.log("sessionToken from getHeaders:", sessionToken);
    //console.log('Canonical Request: ', getCanonicalRequest(sessionToken));
    var canonicalRequest = getCanonicalRequest(sessionToken);
    console.log("canonical Request :", canonicalRequest);
    var x = CryptoJS.SHA256(canonicalRequest);
    x = x.toString(CryptoJS.enc.Hex);
    console.log("x:", x);
    var stringToSign = createStringToSign(x);
    stringToSign = stringToSign.toString(CryptoJS.enc.Hex);
    console.log("stringToSign:", stringToSign);
    var signingKey = getSignatureKey(secretKey, datestamp, regionName, serviceName);
    //signingKey = signingKey.toString(CryptoJS.enc.Hex);
    console.log("signingKey:", signingKey);
    var hash = CryptoJS.HmacSHA256(  stringToSign, signingKey);
    var signature = CryptoJS.SHA256(hash);
    signature = signature.toString(CryptoJS.enc.Hex);
    console.log("signature:", signature);
    var authorizationHeader = algorithm + ' ' + 'Credential=' + accessKey + '/' + credentialScope + ', ' + 'SignedHeaders=' + signed_headers + ', ' + 'Signature=' + signature; 
    console.log("authorizationHeader: ", authorizationHeader);
    var headers = { 
      //'Access-Control-Allow-Origin':"*.*",
     // 'Content-Type': "text/json;charset=UTF-8",
      'X-Amz-Security-Token': sessionToken,
           'X-Amz-Content-Sha256':payload_hash,
            'X-Amz-Date': amzdate, "Authorization": authorizationHeader}
    //console.log("headers:", headers);
    //const article = { title: 'Axios PUT Request Example' };
    var config = {
      method: method,
      url: endpoint, 
      /*headers: {"X-Amz-Security-Token": sessionToken,
      "X-Amz-Content-Sha256":payload_hash,
       "X-Amz-Date": amzdate, "Authorization": authorizationHeader }*/
       headers : headers
    };
    console.log("config:", config);
    //axios.put(endpoint, article,  headers)
    axios(config)
    .then(function (response) {
  console.log(JSON.stringify(response.data));
})
.catch(function (error) {
  console.log(error);
})
  
    //return headers;
    //return "";
}

getHeaders(accessKeyId, secretAccessKey, sessionToken);
//var head =   getHeaders(accessKeyId, secretAccessKey, sessionToken);
//    console.log("get headers :", head);
  ///////////////API call with axios//////////////////////////////////////
  //debugger;
  