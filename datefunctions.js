Date.prototype.yyyymmdd = function() {
    var mm = this.getUTCMonth() + 1; // getMonth() is zero-based
    var dd = this.getUTCDate();
  
    return [this.getFullYear(),
            (mm>9 ? '' : '0') + mm,
            (dd>9 ? '' : '0') + dd
           ].join('');
  };
Date.prototype.yyyymmddThhmmssZ = function()
{
  var mm = this.getUTCMonth() + 1; // getMonth() is zero-based
  var dd = this.getUTCDate();
  var hh = this.getUTCHours();
  var min = this.getUTCMinutes();
  var ss = this.getUTCSeconds();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd, 
          'T',
          (hh>9 ? '': '0') + hh,
          (min>9 ? '': '0') + min,
          (ss>9 ? '': '0') + ss,
          'Z'
         ].join(''); 
}  
 exports.getDateYYYYMMDD = function ()
  {
    var date = new Date();
    return date.yyyymmdd();
  }
exports.getDateYYYYMMDDHHMMSS = function ()
{
    var date = new Date();
    return date.yyyymmddThhmmssZ();
}