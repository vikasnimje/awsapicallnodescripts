require('dotenv').config();
const service = process.env.SERVICE_NAME_CLOUDFORMATION;

const url = process.env.ENDPOINT_POST;
let params = (new URL(url)).searchParams;
//console.log("aws1 ",params);
function encodeRfc3986(urlEncodedStr) {
    return urlEncodedStr.replace(/[!'()*]/g, c => '%' + c.charCodeAt(0).toString(16).toUpperCase())
}

const seenKeys = new Set();
let encodedSearch = [...params].filter(([k]) => {
    if (!k) return false
    if (service === 's3') {
      if (seenKeys.has(k)) return false
      seenKeys.add(k);
    }
    return true
  })
  .map(pair => pair.map(p => encodeRfc3986(encodeURIComponent(p))))
  .sort(([k1, v1], [k2, v2]) => k1 < k2 ? -1 : k1 > k2 ? 1 : v1 < v2 ? -1 : v1 > v2 ? 1 : 0)
  .map(pair => pair.join('='))
  .join('&');

//  console.log("encodedSearch ",encodedSearch);
module.exports = encodedSearch;