/*function printString(string){
    setTimeout(()=> {
        console.log(string)
    }, Math.floor(Math.random() * 100) + 1)
}*/
/*function printString(string, callback){
    setTimeout(()=> {
        console.log(string);
        callback();
    }, Math.floor(Math.random() * 100) + 1)
}*/
function printString(string)
{
    return new Promise((resolve, reject) =>{
        setTimeout(()=>{
            console.log(string);
            resolve();
        },
        Math.floor(Math.random() * 100) + 1
        )  
    })
}

async function printAll()
{
    // callback call
    /*printString("A", ()=> {
        printString("B", ()=> {
            printString("C", ()=>{ })
        })
    });*/

    // primise call 
    /*printString("A")
    .then(()=> printString("B"))
    .then(()=>{
        return printString("C")
    })*/

    // await call
    await printString("A");
    await printString("B");
    await printString("C");
}
printAll();