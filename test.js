const aws = require('aws4fetch');
require('dotenv').config();
const accessKeyId= process.env.ACCESS_KEY;
const secretAccessKey = process.env.SECRET_KEY;
const   sessionToken = process.env.SESSION_TOKEN;
const service = "cloudformation";
const region = "us-east-1";
const url = "https://cloudformation.us-east-1.amazonaws.com/?Action=CreateStack&Version=2010-05-15&Parameters.member.1.ParameterValue=nilmani-yash-demo9&StackName=nilmanisinghs9&Parameters.member.1.ParameterKey=BucketName&TemplateURL=https://iac-sme-test.s3.amazonaws.com/awsa.yml";
/* const method = "POST";
const headers = {
    "Content-Type" : "application/json"
}; */

/* const aws1 = new aws.AwsV4Signer({
    url,
    accessKeyId,     // required, akin to AWS_ACCESS_KEY_ID
    secretAccessKey, // required, akin to AWS_SECRET_ACCESS_KEY
    sessionToken,
    method,    // akin to AWS_SESSION_TOKEN if using temp credentials
    service,         // AWS service, by default parsed at fetch time
    region,   // defaults to 50 – timeout doubles each retry
  }) */


// let params = (new URL(url)).searchParams

// console.log(typeof(params));
// console.log("aws1 ",params);

// function encodeRfc3986(urlEncodedStr) {
//     return urlEncodedStr.replace(/[!'()*]/g, c => '%' + c.charCodeAt(0).toString(16).toUpperCase())
//   }
  
// const seenKeys = new Set();
// //spread operator is used to spread an array, and object literals
// // Spreads [5,6,7,8] as 5,6,7,8
// let encodedSearch2 = [...params];
// console.log("encodedSearch2:", encodedSearch2);
// console.log("typeofencodedsearch2:", typeof(encodedSearch2));
// let encodedSearch = [...params].filter(([k]) => {
//     if (!k) return false
//     if (service === 's3') {
//       if (seenKeys.has(k)) return false
//       seenKeys.add(k);
//     }
//     return true
//   })
//   console.log("encoded serarch:", encodedSearch);
//   encodedSearch.map(pair => pair.map(p => encodeRfc3986(encodeURIComponent(p))));
//   console.log("encoded serarch after map:", encodedSearch);
//   encodedSearch.sort(([k1, v1], [k2, v2]) => k1 < k2 ? -1 : k1 > k2 ? 1 : v1 < v2 ? -1 : v1 > v2 ? 1 : 0);
//   console.log("encoded serarch after sort:", encodedSearch);
//   encodedSearch = encodedSearch.map(pair => pair.join('='));
//   console.log("encoded serarch after map:", encodedSearch);
//   encodedSearch = encodedSearch.join('&');
//   console.log("encoded serarch after join:", encodedSearch);

//   //console.log("encodedSearch ",encodedSearch);
let params = (new URL(url)).searchParams;

console.log("aws1 ",params);
function encodeRfc3986(urlEncodedStr) {
    return urlEncodedStr.replace(/[!'()*]/g, c => '%' + c.charCodeAt(0).toString(16).toUpperCase())
  }
  
const seenKeys = new Set();
let encodedSearch = [...params].filter(([k]) => {
    if (!k) return false
    if (service === 's3') {
      if (seenKeys.has(k)) return false
      seenKeys.add(k);
    }
    return true
  })
  //.map(pair => pair.map(p => encodeRfc3986(encodeURIComponent(p))))
  .map(pair => pair.map(p => encodeURIComponent(p)))
  .sort(([k1, v1], [k2, v2]) => k1 < k2 ? -1 : k1 > k2 ? 1 : v1 < v2 ? -1 : v1 > v2 ? 1 : 0)
  .map(pair => pair.join('='))
  .join('&');

  console.log("encodedSearch ",encodedSearch);